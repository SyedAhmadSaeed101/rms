from rest_framework import serializers
from resume.models import Profile, user_object
from rest_auth.serializers import UserDetailsSerializer
from rest_auth.registration.serializers import RegisterSerializer


class ProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = "__all__"


class ProfileListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Profile
        fields = ('pk', 'first_name', 'last_name')


class UserSerializer(UserDetailsSerializer):

    class Meta:
        model = user_object
        fields = ('is_superuser', 'first_name', 'last_name', 'username', 'id')


# class CustomRegisterSerializer(RegisterSerializer):
#     first_name = serializers.CharField(allow_blank=True)
#     last_name = serializers.CharField(allow_blank=True)
#     profile = ProfileSerializer(required=False)
#
#     class Meta:
#         model = user_object
#         fields = ('first_name', 'last_name', 'email', 'profile', 'pk')
#
#     def save(self, request):
#         user = super().save(request)
#         user.first_name = self.validated_data.get('first_name')
#         user.last_name = self.validated_data.get('last_name')
#         user.save()
#         profile_data = self.validated_data.get('profile', None)
#         if profile_data is not None:
#             profile = Profile.objects.create(**profile_data, user=user)
#             # profile.user = user
#             profile.save()
#         return user
#















