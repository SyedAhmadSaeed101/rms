from django.conf.urls import url, include
from .views import ProfileViewSet, ProfileListViewSet
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'profile', ProfileViewSet)
router.register(r'profilelist', ProfileListViewSet)

urlpatterns = [
    url(r'^accounts/', include('rest_auth.urls')),
    url(r'^accounts/registration', include('rest_auth.registration.urls')),
]

urlpatterns += router.urls

