from django.db import models
from django.contrib.auth import get_user_model


user_object = get_user_model()


class Profile(models.Model):

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    dob = models.DateField(null=True)
    age = models.IntegerField(null=True)
    gender = models.CharField(max_length=1)

    resume = models.FileField(upload_to='resume/%Y/%m/%d/', null=True)








